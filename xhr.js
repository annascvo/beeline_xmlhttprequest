/*Залогиниться с помощью метода https://reqres.in/api/login, в теле запроса указать JSON
    {
        "username": "george.bluth@reqres.in",
        "email": "george.bluth@reqres.in",
        "password": "george"
    }
В заголовке (Content-Type) указать формат отправляемых данных: application/json;charset=utf-8
Извлечь полученный токен и сделать вызов второй функции, передав в нее токен*/
let xmlHttpRequestLogin = function () {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        xhr.open("POST", 'https://reqres.in/api/login', true);
        xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
        let json = JSON.stringify({
            "username": "george.bluth@reqres.in",
            "email": "george.bluth@reqres.in",
            "password": "george"
        })
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status < 400) {
                    let data = JSON.parse(this.responseText);
                    resolve(data);
                } else {
                    reject(new Error('Error'));
                }
            }
        };
        xhr.send(json);
    });
};

xmlHttpRequestLogin()
    .then(function (data) {
        console.log('token:', data.token);

        /*Запросить список первых 10 пользователей с помощью метода https://reqres.in/api/users
        Отключить кэширование с помощью заголовка Cache-Control, 
        выставить русскую локаль с помощью Accept-Language, 
        указать формат отправляемых данных: application/json;charset=utf-8 
        и в заголовке Authorization передать токен полученный из первой функции
        Вывести полученный список email’ов пользователей массивом в консоль
        Распечатать в консоль состояния, которые проходит выполнение запроса с помощью события onreadystatechange */

        let xmlHttpRequestList = function () {
            return new Promise(function (resolve, reject) {
                let xhr = new XMLHttpRequest();
                xhr.open("GET", 'https://reqres.in/api/users?per_page=10', true);
                xhr.setRequestHeader('Cache-Control', 'no-cache');
                xhr.setRequestHeader('Accept-Language', 'ru-RU');
                xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
                xhr.setRequestHeader('Authorization', `${data.token}`);
                
                xhr.onreadystatechange = function () {
                    if(this.readyState === 0){
                        console.log("UNSENT = 0 - начальное состояние")
                    }
                    if(this.readyState === 1){
                        console.log("OPENED = 1 - вызван open")
                    }
                    if(this.readyState === 2){
                        console.log("HEADERS_RECEIVED = 2 - получены заголовки")
                    }
                    if(this.readyState === 3){
                        console.log("LOADING = 3 - загружается тело")
                    }
                    if (this.readyState === 4) {
                        console.log("DONE = 4 - запрос завершён")
                        if (this.status >= 200 && this.status < 400) {
                            let data = JSON.parse(this.responseText);
                            resolve(data);
                        } else {
                            reject(new Error('Error'));
                        }
                    }
                };
                xhr.send();
            });
        };
        
        xmlHttpRequestList()
            .then(function (data) {
                console.log('GETdata', data);
                let email = [];
                data.data.forEach(element => {
                    email.push(element.email);
                });
                console.log('Cписок email’ов пользователей', email);
            }).catch(function (err) {
                console.error(err);
            });


    }).catch(function (err) {
        console.error(err);
    });




